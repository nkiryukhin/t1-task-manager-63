package ru.t1.nkiryukhin.tm.servlet;

import ru.t1.nkiryukhin.tm.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/delete/*")
public class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String id = req.getParameter("id");
        ProjectRepository.getInstance().removeById(id);
        resp.sendRedirect("/projects");
    }

}
