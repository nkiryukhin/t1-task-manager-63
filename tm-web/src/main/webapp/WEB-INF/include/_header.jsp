<html>
    <head>
        <title>TASK MANAGER</title>
    </head>
    <style>
        h1 {
            font-size: 1.6em;
        }

        a {
            color: darkblue;
        }

        select {
            width: 200px;
        }

        input[type="text"] {
            width: 200px;
        }

        input[type="date"] {
            width: 200px;
        }
    </style>

<body>
    <table width="100%" height="100%" border="1" style="border-collapse: collapse" cellpadding="10">
        <tr>
            <td height="35" width="200" nowrap="nowrap" align="center">
                <b>TASK MANAGER</b>
            </td>
            <td width="100%" align="right" padding="10">
                <a href="/projects">PROJECTS</a> | <a href="/tasks">TASKS</a>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="100%" valign="top">