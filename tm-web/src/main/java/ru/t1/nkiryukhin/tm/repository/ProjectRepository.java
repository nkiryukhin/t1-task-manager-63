package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ProjectRepository {

    @NotNull
    private final static ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("DEMO"));
        add(new Project("TEST"));
        add(new Project("ALPHA"));
        add(new Project("BETA"));
    }

    public void create() {
        add(new Project("New Project " + System.currentTimeMillis()));
    }

    public void add(Project project) {
        projects.put(project.getId(), project);
    }

    public void save(Project project) {
        projects.put(project.getId(), project);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(final String id) {
        return projects.get(id);
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

}