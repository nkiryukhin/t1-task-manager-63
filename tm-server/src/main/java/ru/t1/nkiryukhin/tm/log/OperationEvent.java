package ru.t1.nkiryukhin.tm.log;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class OperationEvent {

    @NotNull
    private OperationType type;

    @NotNull
    private Object entity;

    @Nullable
    private String table;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull final OperationType type, @NotNull final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}