package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class TaskRepository {

    @NotNull
    private final static TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("ONE"));
        add(new Task("TWO"));
    }

    public void create() {
        add(new Task("New Task " + System.currentTimeMillis()));
    }

    public void add(final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(final Task task) {
        tasks.put(task.getId(), task);
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(final String id) {
        return tasks.get(id);
    }

    public void removeById(final String id) {
        tasks.remove(id);
    }

}
