package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.IUserEndpoint;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;
import ru.t1.nkiryukhin.tm.dto.request.*;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;
import ru.t1.nkiryukhin.tm.service.PropertyService;

import java.util.List;

import static ru.t1.nkiryukhin.tm.dataDTO.ProjectTestData.*;
import static ru.t1.nkiryukhin.tm.dataDTO.UserTestData.*;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyService();

    static
    {
        propertyService.setHost("localhost");
        propertyService.setPort("8080");
    }

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectOneId;

    private int projectOneIndex;

    @Nullable
    private String projectTwoId;

    private int projectTwoIndex;

    @BeforeClass
    public static void setUp() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_USER_LOGIN);
        loginRequest.setPassword(ADMIN_USER_PASSWORD);
        adminToken = authEndpoint.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        request.setPassword(USUAL_USER_PASSWORD);
        request.setEmail(USUAL_USER_EMAIL);
        userEndpoint.registryUser(request);
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(USUAL_USER_LOGIN);
        userLoginRequest.setPassword(USUAL_USER_PASSWORD);
        userToken = authEndpoint.login(userLoginRequest).getToken();
    }

    @AfterClass
    public static void tearDown() throws AbstractException {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        userEndpoint.removeUser(request);
    }

    @Before
    public void before() throws AbstractException {
        projectOneId = createTestProject(PROJECT_ONE_NAME, PROJECT_ONE_DESCRIPTION);
        projectOneIndex = 0;
        projectTwoId = createTestProject(PROJECT_TWO_NAME, PROJECT_TWO_DESCRIPTION);
        projectTwoIndex = 1;
    }

    @After
    public void after() throws AbstractException {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(projectEndpoint.clearProject(request));
    }

    @NotNull
    private String createTestProject(final String name, final String description) throws AbstractException {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        return projectEndpoint.createProject(projectCreateRequest).getProject().getId();
    }

    @Nullable
    private ProjectDTO findTestProjectById(final String id) throws AbstractException {
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(userToken);
        request.setId(id);
        return projectEndpoint.getProjectById(request).getProject();
    }

    @Test
    public void changeStatusByIdProject() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIdRequest projectCreateRequestNullToken = new ProjectChangeStatusByIdRequest(null);
        projectCreateRequestNullToken.setId(projectOneId);
        projectCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(projectCreateRequestNullToken));
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setId(projectOneId);
        request.setStatus(status);
        Assert.assertNotNull(projectEndpoint.changeProjectStatusById(request));
        @Nullable final ProjectDTO project = findTestProjectById(projectOneId);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearProject() throws AbstractException {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(projectEndpoint.clearProject(request));
        @Nullable ProjectDTO project = findTestProjectById(projectOneId);
        Assert.assertNull(project);
        project = findTestProjectById(projectTwoId);
        Assert.assertNull(project);
    }

    @Test
    public void createProject() throws AbstractException {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(PROJECT_THREE_NAME);
        projectCreateRequest.setDescription(PROJECT_THREE_DESCRIPTION);
        @Nullable ProjectDTO project = projectEndpoint.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_THREE_NAME, project.getName());
        Assert.assertEquals(PROJECT_THREE_DESCRIPTION, project.getDescription());
    }

    @Test
    public void completeByIdProject() throws AbstractException {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken);
        request.setId(projectOneId);
        Assert.assertNotNull(projectEndpoint.completeProjectById(request));
        @Nullable ProjectDTO project = findTestProjectById(projectOneId);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void listProject() throws AbstractException {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        @Nullable final List<ProjectDTO> projects = projectEndpoint.listProject(request).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        for (ProjectDTO project : projects) {
            Assert.assertNotNull(findTestProjectById(project.getId()));
        }
    }

    @Test
    public void removeByIdProject() throws AbstractException {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setId(projectTwoId);
        Assert.assertNotNull(projectEndpoint.removeProjectById(request));
        Assert.assertNull(findTestProjectById(projectTwoId));
    }

    @Test
    public void showByIdProject() throws AbstractException {
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(userToken);
        request.setId(projectOneId);
        @Nullable final ProjectDTO project = projectEndpoint.getProjectById(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOneId, project.getId());
    }

    @Test
    public void startByIdProject() throws AbstractException {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken);
        request.setId(projectOneId);
        Assert.assertNotNull(projectEndpoint.startProjectById(request));
        @Nullable ProjectDTO project = findTestProjectById(projectOneId);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void updateByIdProject() throws AbstractException {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setId(projectOneId);
        request.setName(PROJECT_THREE_NAME);
        request.setDescription(PROJECT_THREE_DESCRIPTION);
        Assert.assertNotNull(projectEndpoint.updateProjectById(request));
        @Nullable ProjectDTO project = findTestProjectById(projectOneId);
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_THREE_NAME, project.getName());
        Assert.assertEquals(PROJECT_THREE_DESCRIPTION, project.getDescription());
    }

}
